package org.booking.service;

import org.booking.dao.FlightsDAO;
import org.booking.domain.Flight;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

class FlightsServiceTest {
    private static FlightsService flightsService;
    static File tempFile = new File("flights_test.bin");

    @BeforeAll
    public static void setUp() {
        FlightsDAO flightsDAO = new FlightsDAO(tempFile);
        flightsService = new FlightsService(flightsDAO);
        flightsService.SaveFlights();
    }

    @AfterAll
    public static void cleanup() {
        tempFile.delete();
    }

    @Test
    public void testGetFlightByCode() {
        try {
            Flight actualFlight = flightsService.getFlightById(1);
            assertEquals(1, actualFlight.id());
            fail("Expected IllegalStateException was not thrown");
        } catch (IllegalStateException e) {
            // Expected exception was thrown
        }
    }


    @Test
    void testGetFlightByCodeNotFound() {
        String airlineCode = "ZZ1000";

        assertThrows(IllegalStateException.class, () -> flightsService.getFlightByCode(airlineCode));
    }
}
