package org.booking.service;

import org.booking.dao.ReservationDAO;
import org.booking.domain.CityEnum;
import org.booking.domain.Flight;
import org.booking.domain.User;
import org.junit.jupiter.api.*;

import java.io.File;
import java.time.LocalDateTime;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ReservationServiceTest {
    private static final File FILE = new File("test_reservations.bin");
    private static ReservationDAO dao;
    private static ReservationService resService;
    private static User testUser1;
    private static User testUser2;
    private static Flight testFlight;


    @BeforeAll
    public static void setup(){
        dao = new ReservationDAO(FILE);
        resService = new ReservationService(dao);
        testUser1 = new User(1,"testuser", "testpass", "test", "test");
        testUser2 = new User(1,"testuser2", "testpass2", "test2", "test");
        testFlight = new Flight(1, "testorigin", LocalDateTime.now(), CityEnum.TOKYO,
                CityEnum.SYDNEY, "TEST", 8);
    }
    @AfterAll
    public static void cleanup() {
        FILE.delete();
    }

    @Test
    @Order(1)
    public void testCreateReservation() {
        resService.createReservation(testUser1, testFlight, 1);
        int prevSize1 = resService.getReservationsByUser(testUser1).size();
        resService.createReservation(testUser1, testFlight, 1);
        int afterSize1 = resService.getReservationsByUser(testUser1).size();

        resService.createReservation(testUser2, testFlight, 1);
        int prevSize2 = resService.getReservationsByUser(testUser2).size();
        resService.createReservation(testUser2, testFlight, 1);
        int afterSize2 = resService.getReservationsByUser(testUser2).size();

        assertEquals(afterSize1, prevSize1 +1);
        assertEquals(afterSize2, prevSize2 +1);
    }

    @Test
    @Order(2)
    public void testGetReservationByParams(){
        assertThrows(IllegalStateException.class, () -> {
            resService.getReservationByParams(3, testUser1);
        });
    }


    @Test
    @Order(3)
    public void testDeleteReservationByUserFail() {
        assertThrows(IllegalStateException.class, () -> {
            resService.deleteReservation(3, testUser1);
        });
    }

    @Test
    @Order(4)
    public void testDeleteReservationByUser() {
        int prevSize = resService.getReservationsByUser(testUser2).size();
        resService.deleteReservation(3, testUser2);
        int afterSize = resService.getReservationsByUser(testUser2).size();
        assertEquals(afterSize, prevSize -1);
    }

}
