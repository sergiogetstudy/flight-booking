package org.booking.service;

import org.booking.dao.UsersDAO;
import org.booking.domain.User;
import org.junit.jupiter.api.*;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

class UsersServiceTest {
    static UsersDAO testNullDAO;
    UsersDAO testNormDAO;
    static File testFile;
    static User user1;
    User user1sameId;
    User user2;
    UsersService testUserService;
    UsersService testUserServiceNull;
    String login = "login";
    String password = "password";

    @BeforeAll
    static void beforeAll() {
        testNullDAO = new UsersDAO(null);
        user1 = new User(1, "name1", "surname1", "login1", "password1");
        testFile = new File("test_users");
    }

    @AfterAll
    static void afterAll() {
        testFile = null;
        testNullDAO = null;
        user1 = null;
    }

    @BeforeEach
    void setUp() {
        testNormDAO = new UsersDAO(testFile);
        user1sameId = new User(1, "name1_1", "surname1_1", "login1_1", "password1_1");
        user2 = new User(2, "name1", "surname1", "login1", "password1");

        testUserService = new UsersService(testNormDAO);
        testUserServiceNull = new UsersService(testNullDAO);
    }


    @AfterEach
    void tearDown() {
        testNormDAO = null;
        testFile.delete();
        testNormDAO = null;
    }

    @Test
    void createUser() {
        assertTrue(testUserService.createUser("name", "surname", "login", "password"));
        assertEquals(1, testNormDAO.readAll().get(0).id());
        assertTrue(testUserService.createUser("name1", "surname1", "login1", "password1"));
        assertEquals(2, testNormDAO.readAll().get(1).id());

        assertFalse(testUserServiceNull.createUser("name", "surname", "login", "password"));
    }

    @Test
    void isUniqueLogin() {
        testNormDAO.create(user1);
        assertFalse(testUserService.isUniqueLogin(user1.login()));
        assertTrue(testUserService.isUniqueLogin("unique"));

        assertTrue(testUserServiceNull.isUniqueLogin("unique"));
    }

    @Test
    void getUserByLogin() {
        testNormDAO.create(user1);
        assertEquals(user1, testUserService.getUserByLogin(user1.login()));
        assertThrows(IllegalStateException.class, () -> testUserService.getUserByLogin("not_exist"));

        assertThrows(IllegalStateException.class, () -> testUserServiceNull.getUserByLogin("not_exist"));
    }
}