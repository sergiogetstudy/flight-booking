package org.booking.controller;

import junit.framework.TestCase;
import org.booking.dao.UsersDAO;
import org.booking.domain.User;
import org.booking.libs.ConsoleIO;
import org.booking.service.UsersService;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class UsersControllerTest extends TestCase {
    static UsersDAO testNullDAO;
    UsersDAO testNormDAO;
    static File testFile;
    static User user1;
    User user2;
    UsersService testUserService;
    UsersService testUserServiceNull;
    UsersController testUsersController;


    @BeforeAll
    static void beforeAll() {
        testNullDAO = new UsersDAO(null);
        user1 = new User(1, "name1", "surname1", "login1", "password1");
        testFile = new File("test_users");
    }

    @AfterAll
    static void afterAll() {
        testFile = null;
        testNullDAO = null;
        user1 = null;
    }

    @BeforeEach
    public void setUp() {
        testNormDAO = new UsersDAO(testFile);
        user2 = new User(2, "name1", "surname1", "login1", "password1");

        testUserService = new UsersService(testNormDAO);
        testUserServiceNull = new UsersService(testNullDAO);
        testUsersController = new UsersController(testUserService);
    }


    @AfterEach
    public void tearDown() {
        testNormDAO = null;
        testFile.delete();
        testNormDAO = null;
        testUsersController = null;
    }

    public static void setInput(String input, UsersController testUsersController) {
        InputStream in = new ByteArrayInputStream(input.getBytes());
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
        testUsersController.setBr(bufferedReader);
        System.setIn(in);
    }

    @Test
    public void testPrintGreeting() {
        setInput("n\n6\n", testUsersController);
        assertDoesNotThrow(() -> testUsersController.printGreeting());
    }

    @Test
    public void testLogin() {
        setInput("login\nn\n6\n", testUsersController);
        assertDoesNotThrow(() -> testUsersController.login());

        setInput("login\ny\nlogin\nn\n6\n", testUsersController);
        assertDoesNotThrow(() -> testUsersController.login());

        testUserService.createUser("name", "surname", "login", "password");

        setInput("login\npassword\n6\n6\n", testUsersController);
        assertDoesNotThrow(() -> testUsersController.login());
    }

    @Test
    public void testAskLogin() {
        setInput("login\nn\n", testUsersController);
        assertThrows(NullPointerException.class, () -> testUsersController.askLogin());

        testUserService.createUser("name", "surname", "login", "password");

        setInput("login\npassword\n", testUsersController);
        assertDoesNotThrow(() -> testUsersController.askLogin());
    }

    @Test
    public void testSignUp() {
        setInput("_\nn\n", testUsersController);
        assertDoesNotThrow(() -> testUsersController.signUp());

        setInput("name\n_\nn\n", testUsersController);
        assertDoesNotThrow(() -> testUsersController.signUp());

        setInput("name\nsurname\nlogin\n_\nn\n", testUsersController);
        assertDoesNotThrow(() -> testUsersController.signUp());

        setInput("name\nsurname\nlogin\npassword\nwrong_password\nn\n", testUsersController);
        assertDoesNotThrow(() -> testUsersController.signUp());

        setInput("name\nsurname\nlogin\npassword\npassword\n", testUsersController);
        assertDoesNotThrow(() -> testUsersController.signUp());

        setInput("name\nsurname\nlogin\nn\n", testUsersController);
        assertDoesNotThrow(() -> testUsersController.signUp());
    }

    @Test
    public void testPrintMenu() {
        setInput("6\n", testUsersController);
        assertDoesNotThrow(() -> testUsersController.printMenu());
    }

    @Test
    public void testPrintUserMenu() {
        setInput("6\n", testUsersController);
        assertDoesNotThrow(() -> testUsersController.printUserMenu(null));

        setInput("name\nsurname\nlogin\npassword\npassword\n", testUsersController);
        testUsersController.signUp();
        setInput("login\npassword\n", testUsersController);
        User user = testUsersController.askLogin();
        setInput("6\n6\n", testUsersController);
        assertDoesNotThrow(() -> testUsersController.printUserMenu(user));
    }
}