package org.booking.controller;

import org.booking.dao.FlightsDAO;
import org.booking.domain.Flight;
import org.booking.service.FlightsService;
import org.junit.jupiter.api.*;

import java.io.File;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class FlightsControllerTest {
    private static FlightsController flightsController;
    static File tempFile = new File("flights_test.bin");

    @BeforeAll
    public static void setup() {
        FlightsDAO flightsDAO = new FlightsDAO(tempFile);
        FlightsService flightsService = new FlightsService(flightsDAO);
        flightsController = new FlightsController(flightsService);
    }

    @AfterAll
    public static void cleanup() { tempFile.delete(); }

    @Test
    void testGetFlightById() {
        Optional<Flight> actualFlight = flightsController.getFlightById(1);

        assertEquals(1, actualFlight.get().id());
    }

    @Test
    void testGetFlightWithInvalidAirlineCode() {
        Optional<Flight> actualFlight = flightsController.getFlight("XYZ789");

        assertFalse(actualFlight.isPresent());
    }

}
