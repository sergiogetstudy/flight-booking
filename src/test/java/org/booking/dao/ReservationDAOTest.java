package org.booking.dao;

import org.booking.domain.CityEnum;
import org.booking.domain.Flight;
import org.booking.domain.Reservation;
import org.booking.domain.User;
import org.junit.jupiter.api.*;

import java.io.File;
import java.time.LocalDateTime;
import java.util.Date;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ReservationDAOTest {
    private static final File FILE = new File("test_reservations.bin");
    private static ReservationDAO dao;
    private static User testUser;
    private static Flight testFlight;
    private static Reservation testReservation;


    @BeforeAll
    public static void setup(){
        dao = new ReservationDAO(FILE);
        testUser = new User(1,"testuser", "testpass", "test", "test");
        testFlight = new Flight(1, "testorigin", LocalDateTime.now(), CityEnum.TOKYO,
                CityEnum.SYDNEY, "TEST", 3);
        testReservation = new Reservation(1, testFlight, 2, testUser);
    }
    @AfterAll
    public static void cleanup() {
        FILE.delete();
    }
    @Test
    @Order(1)
    public void testCreate() {
        assertTrue(dao.create(testReservation));
    }

    @Test
    @Order(2)
    public void testRead() {
        Reservation reservation = dao.read(testReservation.id());
        assertNotNull(reservation);
        assertEquals(testReservation, reservation);
    }

    @Test
    @Order(3)
    public void testReadAll() {
        assertEquals(1, dao.readAll().size());
    }

}
