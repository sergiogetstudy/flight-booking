package org.booking.dao;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.booking.domain.User;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

public class UsersDAOTest {
    static UsersDAO testNullDAO;
    UsersDAO testNormDAO;
    static File testFile;
    static User user1;
    static List<User> users1expected;
    User user1sameId;
    User user2;

    @BeforeAll
    static void init() {
        testNullDAO = new UsersDAO(null);
        user1 = new User(1, "name1", "surname1", "login1", "password1");
        users1expected = new ArrayList<>();
        users1expected.add(user1);
        testFile = new File("test_users");
    }
    @AfterAll
    static void finish() {
        testFile = null;
        testNullDAO = null;
        user1 = null;
        users1expected = null;
    }
    @BeforeEach
    public void setUp() throws Exception {
        testNormDAO = new UsersDAO(testFile);
        user1sameId = new User(1, "name1_1", "surname1_1", "login1_1", "password1_1");
        user2 = new User(2, "name1", "surname1", "login1", "password1");
    }

    @AfterEach
    public void tearDown() throws Exception {
        testNormDAO = null;
        testFile.delete();
    }

    @Test
    public void testCreate() {
        assertFalse(testNormDAO.create(null));

        assertEquals(new ArrayList<User>(), testNormDAO.readAll());

        boolean isCreatedUser1 = testNormDAO.create(user1);
        assertEquals(users1expected, testNormDAO.readAll());
        assertTrue(isCreatedUser1);

        testNullDAO.create(user1);
        assertEquals(new ArrayList<User>(), testNullDAO.readAll());
    }

    @Test
    public void testUpdate() {
        testNormDAO.create(user1);

        boolean isUpdatedNotExist = testNormDAO.update(user2);
        assertFalse(isUpdatedNotExist);
        assertEquals(users1expected, testNormDAO.readAll());

        List<User> updatedList = new ArrayList<>();
        updatedList.add(user1sameId);

        boolean isUpdatedTheSame = testNormDAO.update(user1sameId);
        assertTrue(isUpdatedTheSame);
        assertEquals(updatedList, testNormDAO.readAll());

        assertFalse(testNullDAO.update(user1));
    }

    @Test
    public void testDelete() {
        assertFalse(testNormDAO.delete(1));

        testNormDAO.create(user1);
        assertTrue(testNormDAO.delete(user1.id()));

        assertEquals(new ArrayList<>(), testNormDAO.readAll());

        assertFalse(testNullDAO.delete(1));
    }

    @Test
    public void testRead() {
        assertThrows(IllegalStateException.class, () -> testNormDAO.read(1));
        testNormDAO.create(user1);
        User existUser = testNormDAO.read(user1.id());
        assertEquals(user1, existUser);

        assertThrows(IllegalStateException.class, () -> testNullDAO.read(1));
    }
}