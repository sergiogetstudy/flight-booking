package org.booking.dao;

import org.booking.domain.CityEnum;
import org.booking.domain.Flight;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class FlightsDAOTest {
    private static final File FILE = new File("test_flights.bin");
    private static FlightsDAO dao;
    private static Flight testFlight;

    @BeforeAll
    public static void setup(){
        dao = new FlightsDAO(FILE);
        int id = 1;
        testFlight = new Flight(id++, "testorigin", LocalDateTime.now(), CityEnum.TOKYO,
                CityEnum.SYDNEY, "TEST", 3);
    }

    @AfterAll
    public static void cleanup() {
        FILE.delete();
    }

    @Test
    public void testReadAll() {
        dao.create(testFlight);
        assertEquals(1, dao.readAll().size());
        assertEquals(testFlight, dao.readAll().get(0));
    }

    @Test
    public void testRead() {
        try {
            Flight flight = dao.read(testFlight.id());
            assertEquals(testFlight, flight);
            fail("Expected IllegalStateException was not thrown");
        } catch (IllegalStateException e) {
            // Expected exception was thrown
        }
    }
}
