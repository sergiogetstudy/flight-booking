package org.booking.dao;

import org.booking.domain.Flight;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FlightsDAO extends FileDAO<Flight> {
    public FlightsDAO(File file) {
        super(file);
    }
}
