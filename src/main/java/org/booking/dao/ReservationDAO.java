package org.booking.dao;

import org.booking.domain.Reservation;
import org.booking.domain.User;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

public class ReservationDAO  extends FileDAO<Reservation>{
    public ReservationDAO(File file) {
        super(file);
    }

    public void delete(Reservation reservation) throws IllegalStateException{
        List<Reservation> dataList = loadDataFromFile();
        if(!dataList.contains(reservation)) throw new IllegalStateException();
        List<Reservation> output = dataList.stream().filter(o -> !o.equals(reservation)).collect(Collectors.toList());

        saveDataToFile(output);
    }

    public Reservation read(int id, User user) throws IllegalStateException {
        return loadDataFromFile().stream()
                .filter(obj -> obj.id() == id && obj.getUser().equals(user))
                .findFirst()
                .orElseThrow(IllegalStateException::new);
    }
}
