package org.booking.dao;

import org.booking.domain.Identifiable;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public abstract class FileDAO <T extends Identifiable> implements DAO<T>{
    File file;

    public FileDAO(File file) {
        this.file = file;
    }
    public List<T> loadDataFromFile() {
        ArrayList<T> dataList;
        try(ObjectInputStream in = new ObjectInputStream(new FileInputStream(file))) {
            dataList = (ArrayList<T>) in.readObject();
        } catch (Exception ex) {
            dataList = new ArrayList<>();
        }
        return dataList;
    }

    public boolean saveDataToFile(List<T> data) {
        try(ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file))) {
            out.writeObject(data);
            return true;
        } catch (IOException | NullPointerException e) {
            return false;
        }
    }

    @Override
    public T read(int id) throws IllegalStateException {
        return loadDataFromFile().stream()
                .filter(obj -> obj.id() == id)
                .findFirst()
                .orElseThrow(IllegalStateException::new);
    }

    @Override
    public boolean create(T obj) {
        List<T> dataList = loadDataFromFile();
        if(obj == null) return false;
        dataList.add(obj);
        return saveDataToFile(dataList);
    }
    @Override
    public List<T> readAll() {
        return loadDataFromFile();
    }
    @Override
    public boolean delete(int id) {
        List<T> dataList = loadDataFromFile();
        List<T> output = dataList.stream().filter(o -> o.id() != id).collect(Collectors.toList());
        if(dataList.size() - 1 != output.size()) return false;
        saveDataToFile(output);
        return true;
    }
}
