package org.booking.dao;

import org.booking.domain.User;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UsersDAO extends FileDAO<User> {

    public UsersDAO(File file) {
        super(file);
    }
}
