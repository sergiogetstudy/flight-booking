package org.booking.dao;

import org.booking.domain.Identifiable;
import org.booking.domain.User;

import java.util.List;

public interface DAO<T extends Identifiable> {

    boolean create(T a);

    T read(int id) throws Exception;

    List<T> readAll();

    default boolean update(T a) {
        return delete(a.id()) && create(a);
    }

    boolean delete(int id);

}
