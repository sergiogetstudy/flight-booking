package org.booking.service;

import org.booking.dao.ReservationDAO;
import org.booking.domain.Flight;
import org.booking.domain.Reservation;
import org.booking.domain.User;

import java.util.List;
import java.util.stream.Collectors;

public class ReservationService {
    private final ReservationDAO resDAO;
    private List<Reservation> reservations;

    public ReservationService(ReservationDAO resDAO) {
        this.resDAO = resDAO;
    }
    public List<Reservation> getReservationsByUser(User user) throws IllegalStateException {
        List <Reservation> result =  resDAO.readAll().stream()
                .filter(res -> res.getUser().equals(user)).collect(Collectors.toList());
        if(result.size() == 0) throw new IllegalStateException();

        return result;
    }

    public Reservation getReservationByParams(int id, User user) throws IllegalStateException {
        return resDAO.read(id, user);
    }

    private boolean saveReservation(Reservation reservation) {
       return resDAO.create(reservation);
    }

    public boolean createReservation(User user, Flight flight, int passengers) {
        int id = resDAO.readAll().stream().mapToInt(Reservation::id).max().orElse(0) + 1;
        Reservation newRes = new Reservation(id, flight, passengers, user);
        flight.bookSeats(passengers);
        return saveReservation(newRes);
    }
    public void deleteReservation(int id, User user) throws IllegalStateException{
           try {
               resDAO.delete(getReservationByParams(id, user));
           } catch (IllegalStateException ex) {
               throw new IllegalStateException();
           }
    }
}
