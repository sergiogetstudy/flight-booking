package org.booking.service;

import org.booking.dao.DAO;
import org.booking.domain.User;

import java.util.List;

public class UsersService {
    private final DAO<User> usersDAO;
    public UsersService(DAO<User> usersDAO) {
        this.usersDAO = usersDAO;
    }

    public boolean createUser(String name, String surname, String login, String password) {

        int id = usersDAO.readAll().stream().mapToInt(User::id).max().orElse(0) + 1;
        User user = new User(id, name, surname, login, password);

        return usersDAO.create(user);
    }

    public boolean isUniqueLogin(String login) {
        return usersDAO.readAll().stream().map(User::login).noneMatch(l -> l.equals(login));
    }

    User getUserById(int id) throws Exception {
        return usersDAO.read(id);
    }

    public User getUserByLogin(String login) throws IllegalStateException {
        return usersDAO.readAll().stream()
                .filter(user -> user.login().equals(login))
                .findFirst()
                .orElseThrow(IllegalStateException::new);
    }
}
