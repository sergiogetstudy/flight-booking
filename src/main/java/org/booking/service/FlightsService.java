package org.booking.service;

import org.booking.dao.FlightsDAO;
import org.booking.domain.Flight;
import org.booking.libs.FlightsGenerator;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class FlightsService {
    private final FlightsDAO flightsDAO;
    private List<Flight> flights;

    public FlightsService(FlightsDAO flightsDAO) {
        this.flightsDAO = flightsDAO;
        SaveFlights();
    }

    public void SaveFlights() {
        FlightsGenerator flightsGenerator = new FlightsGenerator(100);
        flights = flightsGenerator.getFlights();
        flightsDAO.saveDataToFile(flights);
    }

    public List<Flight> readAll() {
        return flightsDAO.readAll();
    }

    public List<Flight> getFlightsByParams(String originCity, String destinationCity, LocalDate date, int passengers) {
        return readAll().stream().filter(flight -> flight.getOriginCity().name().equals(originCity)
                && flight.getDestinationCity().name().equals(destinationCity)
                && flight.getDate().toLocalDate().equals(date.atStartOfDay().toLocalDate())
                && flight.getAvailableSeats() >= passengers).collect(Collectors.toList());
    }

    public Flight getFlightByCode(String airlineCode) throws IllegalStateException {
        return flightsDAO.readAll().stream()
                .filter(flight -> flight.getAirlineCode().equals(airlineCode))
                .findFirst()
                .orElseThrow(IllegalStateException::new);
    }

    public Flight getFlightById(int id) throws IllegalStateException {
        return flightsDAO.read(id);
    }
}
