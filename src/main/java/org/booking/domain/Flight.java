package org.booking.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Flight implements Identifiable, Serializable {
    private final int id;
    private String airlineCode;
    private LocalDateTime date;
    private CityEnum originCity;
    private CityEnum destinationCity;
    private String airlineName;
    private int availableSeats;

    public Flight(int id, String airlineCode, LocalDateTime date, CityEnum originCity, CityEnum destinationCity,
                  String airlineName, int availableSeats) {
        this.id = id;
        this.airlineCode = airlineCode;
        this.date = date;
        this.originCity = originCity;
        this.destinationCity = destinationCity;
        this.airlineName = airlineName;
        this.availableSeats = availableSeats;
    }

    public String getAirlineCode() { return airlineCode; }
    public LocalDateTime getDate() { return date; }
    public CityEnum getOriginCity() { return originCity; }

    public CityEnum getDestinationCity() { return destinationCity; }

    public String getAirlineName() { return airlineName; }

    public int getAvailableSeats() {
        return availableSeats;
    }

    public void bookSeats(int numberOfSeats) {
        availableSeats -= numberOfSeats;
    }

    private String getDateFormated() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        return date.format(formatter);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb
                .append(id())
                .append(" | ")
                .append(airlineCode)
                .append(" | ")
                .append(getDateFormated())
                .append(" | ")
                .append(originCity)
                .append(" ---> ")
                .append(destinationCity)
                .append(" | ")
                .append(airlineName);
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Flight flight = (Flight) obj;
        return airlineCode.equals(flight.airlineCode);
    }

    @Override
    public int hashCode() {
        return airlineCode.hashCode();
    }

    @Override
    public int id() {
        return id;
    }
}
