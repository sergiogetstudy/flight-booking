package org.booking.domain;

public interface Identifiable {
    int id();
}

