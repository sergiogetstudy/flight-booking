package org.booking.domain;

public enum AirlineEnum {
    AMERICAN_AIRLINES("AA"),
    AIR_FRANCE("AF"),
    UKRAINE_INTERNATIONAL_AIRLINES("UIA"),
    BRITISH_AIRWAYS("BA"),
    TURKISH_AIRLINES("TK"),
    LUFTHANSA("LH"),
    AZAL("J2"),
    RYANAIR("FR"),
    AIR_CHINA("CA"),
    QATAR_AIRWAYS("QR");

    private final String code;
    AirlineEnum(String code) { this.code = code; }

    public String getCode() { return code; }
}
