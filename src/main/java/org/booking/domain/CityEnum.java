package org.booking.domain;

public enum CityEnum {
    NEW_YORK,
    LONDON,
    PARIS,
    TOKYO,
    BEIJING,
    KYIV,
    SYDNEY,
    LOS_ANGELES,
    CHICAGO,
    MIAMI
}
