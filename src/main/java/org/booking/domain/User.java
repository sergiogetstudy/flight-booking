package org.booking.domain;

import java.io.Serializable;

public class User implements Identifiable, Serializable {
    private final int id;
    private final String name;
    private final String surname;
    private final String login;
    private final String password;

    public User(int id, String name, String surname, String login, String password) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.login = login;
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    @Override
    public int id() {
        return this.id;
    }

    @Override
    public boolean equals(Object that0){
        if(that0 == null) return false;
        if (this == that0) return true;
        if(!(that0 instanceof User)) return false;
        User that = (User) that0;
        return this.id == that.id
                && this.name.equals(that.name)
                && this.surname.equals(that.surname);
    }
    public String login() {
        return this.login;
    }
    public String password() {
        return this.password;
    }
    public String name() {
        return this.name;
    }
}
