package org.booking.domain;

import java.io.Serializable;

public class Reservation implements Identifiable, Serializable {

    private final int id;
    private final Flight flight;
    private final int passengers;
    private final User user;

    public Reservation(int id, Flight flight, int passengers, User user) {
        this.id = id;
        this.flight = flight;
        this.passengers = passengers;
        this.user = user;
    }
    @Override
    public int id() {
        return id;
    }

    @Override
    public String toString(){
        return String.format("RESERVATION №%d\nFLIGHT:\n%s\nPASSENGERS: %d\n\n", id, flight, passengers);
    }

    @Override
    public boolean equals(Object that0){
        if(that0 == null) return false;
        if (this == that0) return true;
        if(!(that0 instanceof Reservation)) return false;
        Reservation that = (Reservation) that0;
        return this.id == that.id;
    }

    public User getUser(){
        return user;
    }
}
