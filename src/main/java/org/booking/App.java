package org.booking;


import org.booking.controller.UsersController;
import org.booking.dao.DAO;
import org.booking.dao.UsersDAO;
import org.booking.domain.User;
import org.booking.libs.FileUtils;
import org.booking.service.UsersService;

import java.io.File;

public class App {
    public static void main( String[] args ) {

        FileUtils.createDbDirectory();

        DAO<User> usersDAO = new UsersDAO(new File("db/users.bin"));
        UsersService usersService = new UsersService(usersDAO);

        UsersController console = new UsersController(usersService);

        console.printGreeting();
    }
}
