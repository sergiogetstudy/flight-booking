package org.booking.libs;

import java.io.BufferedReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public class ConsoleIO {
    public static void printLine(String line) {
        System.out.println(line);
    }
    public static void print(String line) {
        System.out.print(line);
    }
    public static int askInt(BufferedReader br, String message, String messageFallback) {

        while (true) {
            print(message);
            try {
                return Integer.parseInt(br.readLine().trim());
            } catch (IOException ioe) {
                ioe.printStackTrace();
            } catch (NumberFormatException nfe) {
                ConsoleIO.printLine(messageFallback);
            }
        }
    }
    public static String askString(BufferedReader br, String message) {
        while (true) {
            print(message);
            try {
                return br.readLine().trim();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }
    public static boolean askConfirm(BufferedReader br, String message, String yes, String no, String messageFallback) {
        while (true) {
            print(message);
            try {
                String input = br.readLine().trim();
                if (formatString(input).equals(formatString(yes))) return true;
                if (formatString(input).equals(formatString(no))) return false;
                printLine(messageFallback);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    public static LocalDate askDate(BufferedReader br, String message, String messageFallback) {

        while (true) {
            print(message);
            try {
                return DateFormatterInput.format(br.readLine().trim());
            } catch (IOException ioe) {
                ioe.printStackTrace();
            } catch (DateTimeParseException ex) {
                ConsoleIO.printLine(messageFallback);
            }
        }
    }
    private static String formatString(String str) {
        return str.toLowerCase().trim();
    }
}
