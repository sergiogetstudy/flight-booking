package org.booking.libs;

import org.booking.domain.AirlineEnum;
import org.booking.domain.CityEnum;
import org.booking.domain.Flight;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class FlightsGenerator {
    private static int numFlights;
    private List<Flight> flights;
    private static int count = 0;

    public FlightsGenerator(int numFlights) {
        this.numFlights = numFlights;
        this.flights = generateFlightsList();
    }

    public List<Flight> getFlights() {
        return flights;
    }

    public static List<Flight> generateFlightsList() {
        List<Flight> flights = new ArrayList<>();

        for (int i = 0; i < numFlights; i++) {
            flights.add(generateFlight());
        }

        return flights;
    }

    private static AirlineEnum getRandomAirline() {
        AirlineEnum[] airlines = AirlineEnum.values();
        return airlines[ThreadLocalRandom.current().nextInt(airlines.length)];
    }

    private static String getAirlineCode() {
        StringBuilder airlineCodeBuilder = new StringBuilder(getRandomAirline().getCode());
        Random random = new Random();

        while (airlineCodeBuilder.length() < 5) {
            airlineCodeBuilder.append(random.nextInt(10));
        }

        return airlineCodeBuilder.substring(0, 5);
    }

    private static String getAirlineName() {
        return getRandomAirline().name();
    }

    private static CityEnum getRandomCity(Optional<CityEnum> excludeCity) {
        Random random = new Random();
        CityEnum[] cities = CityEnum.values();
        CityEnum destination;
        do {
            destination = cities[random.nextInt(cities.length)];
        } while (excludeCity.isPresent() && destination == excludeCity.get());
        return destination;
    }

//    private static String getRandomDate() {
//        LocalDateTime startDate = LocalDateTime.now();
//        LocalDateTime endDate = startDate.truncatedTo(ChronoUnit.DAYS).plusDays(7);
//        long randomMinutes = ThreadLocalRandom.current().nextLong(0, ChronoUnit.MINUTES.between(startDate, endDate));
//        LocalDateTime randomDateTime = startDate.plusMinutes(randomMinutes);
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
//        return formatter.format(randomDateTime);
//    }
    private static LocalDateTime getRandomDate() {
        LocalDateTime startDate = LocalDateTime.now();
        LocalDateTime endDate = startDate.truncatedTo(ChronoUnit.DAYS).plusDays(7);
        long randomMinutes = ThreadLocalRandom.current().nextLong(0, ChronoUnit.MINUTES.between(startDate, endDate));
        return startDate.plusMinutes(randomMinutes);
    }


    private static Flight generateFlight() {
        Random random = new Random();
        count++;

        Optional<CityEnum> prevCity = Optional.empty();

        CityEnum originCity = getRandomCity(prevCity);
        CityEnum destinationCity = getRandomCity(Optional.of(originCity));
        String aCode = getAirlineCode();
        String aName = getAirlineName();
        LocalDateTime date = getRandomDate();
        int availableSeats = random.nextInt(50) + 50;
        return new Flight(count, aCode, date, originCity, destinationCity, aName, availableSeats);
    }
}
