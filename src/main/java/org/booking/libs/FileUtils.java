package org.booking.libs;

import java.io.File;

public class FileUtils {
    public static void createDbDirectory() {
        File dbDir = new File("db");
        if (!dbDir.exists()) {
            dbDir.mkdirs();
        }
    }
}
