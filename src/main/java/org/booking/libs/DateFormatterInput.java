package org.booking.libs;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoField;

public class DateFormatterInput {
    public static LocalDate format(String stringDate) throws DateTimeParseException {
        DateTimeFormatter formatter = new DateTimeFormatterBuilder()
                .appendPattern("uuuu/M/d")
                .parseDefaulting(ChronoField.MONTH_OF_YEAR, 1)
                .parseDefaulting(ChronoField.DAY_OF_MONTH, 1)
                .toFormatter();
        return LocalDate.parse(stringDate, formatter);
    }
}
