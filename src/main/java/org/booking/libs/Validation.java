package org.booking.libs;

import java.io.BufferedReader;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

public class Validation {
    public static void compareInputAndValue(BufferedReader br, String message, String messageFallback, String value, BiPredicate<String, String> predicate) throws NullPointerException {
        while(true) {
            String input = ConsoleIO.askString(br, message);
            try {
                if(predicate.test(input, value)) return;
                else throw new IllegalStateException();
            } catch (IllegalStateException ise) {
                ConsoleIO.printLine(messageFallback);
                boolean isContinue = ConsoleIO.askConfirm(br,"Спробувати знову? (Y/N): ", "Y", "N", "Невірний символ");
                if (!isContinue) throw new NullPointerException();
            }
        }
    }

    public static String askAndCheckValue(BufferedReader br, String message, String messageFallback, Predicate<String> predicate) throws NullPointerException {
        while(true) {
            String input = ConsoleIO.askString(br, message);
            try {
                if(predicate.test(input)) return input;
                else throw new IllegalStateException();
            } catch (IllegalStateException ise) {
                ConsoleIO.printLine(messageFallback);
                boolean isContinue = ConsoleIO.askConfirm(br,"Спробувати знову? (Y/N): ", "Y", "N", "Невірний символ");
                if (!isContinue) throw new NullPointerException();
            }
        }
    }

    public static String askAndCheckValue(BufferedReader br, String message, String messageFallback, Predicate<String> predicate, int validLength, String messageLengthFallback) throws NullPointerException {
        while(true) {
            try {
                String input = askAndCheckValue(br, message, messageFallback, predicate);
                if(validLength <= input.length()) return input;
                else throw new IllegalStateException();
            } catch (IllegalStateException ise) {
                ConsoleIO.printLine(messageLengthFallback);
                boolean isContinue = ConsoleIO.askConfirm(br,"Спробувати знову? (Y/N): ", "Y", "N", "Невірний символ");
                if (!isContinue) throw new NullPointerException();
            }
        }
    }
}
