package org.booking.controller;

import org.booking.domain.Flight;
import org.booking.libs.ConsoleIO;
import org.booking.service.FlightsService;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public class FlightsController {
    private FlightsService flightsService;

    public FlightsController(FlightsService flightsService) {
        this.flightsService = flightsService;
    }

    public void readAll() {
        List<Flight> flights = flightsService.readAll();
        for (Flight flight : flights) {
            ConsoleIO.printLine(String.valueOf(flight));
        }
    }

    public void getFlightByCode(String airlineCode) {
        Flight flight = flightsService.getFlightByCode(airlineCode);
        ConsoleIO.printLine(String.valueOf(flight));
    }

    public void getFlightSchedule() {
        ConsoleIO.printLine("[Онлайн-табло...]");
        readAll();
    }

    public void getFlightInfo() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in, StandardCharsets.UTF_8));
        String flightInfo;

        while (true) {
            flightInfo = ConsoleIO.askString(br, "Введіть номер рейсу або 0, щоб повернутись до попереднього меню: ");

            if (flightInfo.equals("0")) {
                break;
            }

            if (!flightInfo.isEmpty()) {
                ConsoleIO.printLine("[Інформація про рейс...]");

                try {
                    getFlightByCode(flightInfo);
                    break;
                } catch (IllegalArgumentException ex) {
                    ConsoleIO.printLine("Помилка: Рейс з таким номером не існує. Будь ласка, спробуйте ще раз.");
                }
            }
        }
    }
    public Optional<Flight> getFlight(String airlineCode){
        try {
            return Optional.of(flightsService.getFlightByCode(airlineCode));
        } catch (IllegalStateException ex) {
            ConsoleIO.printLine("Невірний номер рейсу. Спробуйте ще раз");
            return Optional.empty();
        }
    }

    public boolean getFlightsByParams(String originCity, String destinationCity, LocalDate date, int passengers) {
        List<Flight> searchResult = flightsService.getFlightsByParams(originCity.toUpperCase(),
                destinationCity.toUpperCase(), date, passengers);

        if(searchResult.size() != 0) {
            searchResult.stream().forEach(flight -> ConsoleIO.printLine(flight.toString()));
            return true;
        }
        else {
            ConsoleIO.printLine("Нажаль рейси за данними параметрами відсутні");
            return false;
        }
    }

    public Optional<Flight> getFlightById(int id) {
        try {
            return Optional.of(flightsService.getFlightById(id));
        } catch (IllegalStateException ex) {
            ConsoleIO.printLine("Невірний номер рейсу. Спробуйте ще раз");
            return Optional.empty();
        }
    }
}
