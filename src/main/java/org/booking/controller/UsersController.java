package org.booking.controller;

import org.booking.dao.FlightsDAO;
import org.booking.dao.ReservationDAO;
import org.booking.domain.User;
import org.booking.libs.ConsoleIO;
import org.booking.libs.Validation;
import org.booking.service.FlightsService;
import org.booking.service.ReservationService;
import org.booking.service.UsersService;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

public class UsersController {
    UsersService usersService;
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in, StandardCharsets.UTF_8));
    FlightsController flightsController;
    ReservationController reservationController;

    public void setBr(BufferedReader br) {
        this.br = br;
    }

    public UsersController(UsersService usersService) {
        this.usersService = usersService;
        FlightsDAO flightsDAO = new FlightsDAO(new File("db/flights.bin"));
        FlightsService flightService = new FlightsService(flightsDAO);
        flightsController = new FlightsController(flightService);

        ReservationDAO reservationDAO = new ReservationDAO(new File("db/reservations.bin"));
        ReservationService reservationService = new ReservationService(reservationDAO);
        reservationController = new ReservationController(reservationService);
    }

    public void printGreeting() {
        ConsoleIO.printLine("");
        ConsoleIO.printLine("Вітаємо в консольних авіалініях! \uD83D\uDE0E");
        ConsoleIO.printLine("");
        boolean hasLogin = ConsoleIO.askConfirm(br, "Маєте обліковий запис? (Y/N): ", "Y", "N", "Невірний символ \uD83D\uDE41");
        if (hasLogin) login();
        else printMenu();
    }
    public void login() {
        try {
            User user = askLogin();
            Validation.compareInputAndValue(br,"Введіть ваш пароль: ", "Невірний пароль \uD83D\uDE41", user.password(), String::equals);
            ConsoleIO.printLine("");
            printUserMenu(user);
        } catch (NullPointerException npe) {
            ConsoleIO.printLine("Вітаємо, незнайомцю!");
            printMenu();
        }
    }
    public User askLogin() throws IllegalStateException {
        while(true) {
            String login = ConsoleIO.askString(br, "Введіть ваш логін: ");
            try {
                return usersService.getUserByLogin(login);
            } catch (IllegalStateException ise) {
                ConsoleIO.printLine("Невірний логін \uD83D\uDE41");
                boolean isContinue = ConsoleIO.askConfirm(br,"Спробувати знову? (Y/N): ", "Y", "N", "Невірний символ \uD83D\uDE41");
                if (!isContinue) throw new NullPointerException();
            }
        }
    }

    public void signUp() {
        try {
            String name = Validation.askAndCheckValue(
                    br,
                    "Ваше ім'я: ",
                    "Ім'я має містити мінімум два символи",
                    x -> x.length() >= 2);

            String surname = Validation.askAndCheckValue(
                    br,
                    "Ваше прізвище: ",
                    "Прізвище має містити мінімум два символи",
                    x -> x.length() >= 2);

            String login = Validation.askAndCheckValue(
                    br,
                    "Ваш логін: ",
                    "Такий логін вже існує",
                    usersService::isUniqueLogin,
                    4,
                    "Логін має містити мінімум чотири символи");

            String password = Validation.askAndCheckValue(
                    br,
                    "Ваш пароль: ",
                    "Пароль має містити мінімум чотири символи",
                    x -> x.length() >= 4);

            Validation.compareInputAndValue(
                    br,
                    "Повторіть пароль: ",
                    "Невірний пароль \uD83D\uDE41",
                    password,
                    String::equals);

            usersService.createUser(name, surname, login, password);
        } catch (NullPointerException ignored) {}
    }

    public void printMenu() {
        while(true) {
            String[] userMenuItem = new String[] {
                    "1. \uD83D\uDCF0 Онлайн-табло",
                    "2. \uD83D\uDEEB Інформація про рейс",
                    "3. \uD83D\uDD0E Пошук та бронювання рейсу",
                    "4. \uD83D\uDD11 Вхід в обліковий запис",
                    "5. \u2795 Створити обліковий запис",
                    "6. \uD83D\uDEA9 Вихід"
            };
            ConsoleIO.printLine("");
            Arrays.asList(userMenuItem).forEach(ConsoleIO::printLine);
            ConsoleIO.printLine("");
            int choice = ConsoleIO.askInt(br, "Введіть номер меню: ", "Номер має бути числом");

            switch (choice) {
                case 1:
                    flightsController.getFlightSchedule();

                    break;

                case 2:
                    flightsController.getFlightInfo();
                    break;
                case 3:
                    ConsoleIO.printLine("[Пошук та бронювання рейсу...]");
                    String originCity = ConsoleIO.askString(br, "Введіть місто відправлення: ");
                    String destinationCity = ConsoleIO.askString(br, "Введіть місто призначення: ");
                    LocalDate date = ConsoleIO.askDate(br, "Введіть дату в форматі 'yyyy/mm/dd': ",
                            "Не вдалося розпізнати дату, дотримуйтеся поданого формату 'yyyy/mm/dd'!");
                    int passengers = ConsoleIO.askInt(br, "Кількість пасажирів: "
                            , "Введіть число");
                    if (flightsController.getFlightsByParams(originCity, destinationCity, date, passengers)) {
                        ConsoleIO.printLine("Для бронювання рейсу авторизуйтесь або створіть новий профіль");
                    }

                    break;

                case 4:
                    //OK вхід в обліковий запис
                    login();
                    return;

                case 5:
                    //ОК створити обліковий запис
                    signUp();
                    break;

                case 6:
                    //OK
                    try {
                        br.close();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                    return;
                default:
                    //OK
                    ConsoleIO.printLine("");
                    ConsoleIO.printLine("Невірний номер, повторіть спробу: ");
                    ConsoleIO.printLine("");
            }
        }
    }
    public void printUserMenu(User user) {
        if (user == null) {
            ConsoleIO.printLine("Сталася помилка: користувача не знайдено");
            return;
        };
        String[] menuItems = new String[] {
                "1. \uD83D\uDCF0 Онлайн-табло",
                "2. \uD83D\uDEEB Інформація про рейс",
                "3. \uD83D\uDD0E Пошук та бронювання рейсу",
                "4. \uD83D\uDEAB Відмінити бронювання",
                "5. \uD83D\uDCB6 Мої рейси",
                "6. \uD83D\uDD12 Вийти з облікового запису",
        };

        while(true) {
            ConsoleIO.printLine("Вітаємо, " + user.name() + "!");
            ConsoleIO.printLine("");
            Arrays.asList(menuItems).forEach(ConsoleIO::printLine);
            ConsoleIO.printLine("");
            int choice = ConsoleIO.askInt(br, "Введіть номер меню: ", "Номер має бути числом");

            switch (choice) {
                case 1:
                    flightsController.getFlightSchedule();
                    break;

                case 2:
                    flightsController.getFlightInfo();
                    break;
                case 3:
                    ConsoleIO.printLine("[Пошук та бронювання рейсу...]");

                    String originCity = ConsoleIO.askString(br, "Введіть місто відправлення: ");
                    String destinationCity = ConsoleIO.askString(br, "Введіть місто призначення: ");
                    LocalDate date = ConsoleIO.askDate(br, "Введіть дату в форматі 'yyyy/mm/dd': ",
                            "Не вдалося розпізнати дату, дотримуйтеся поданого формату 'yyyy/mm/dd'!");
                    int passengers = ConsoleIO.askInt(br, "Кількість пасажирів: "
                            , "Введіть число");

                    if (flightsController.getFlightsByParams(originCity, destinationCity, date, passengers)) {
                        String selectedFlightAirLineCode = ConsoleIO.askString(br, "Введіть номер рейсу: ");
                        flightsController.getFlight(selectedFlightAirLineCode).ifPresent(flight -> reservationController
                                .createReservation(user, flight, passengers));
                    }

                    break;

                case 4:
                    ConsoleIO.printLine("[Відмінити бронювання...]");
                    int reservationId = ConsoleIO.askInt(br, "Введіть номер бронювання: ", "Введіть число!");
                    reservationController.deleteReservation(reservationId, user);
                    break;

                case 5:
                    ConsoleIO.printLine("[Мої рейси...]");
                    reservationController.printReservationsByUser(user);
                    break;

                case 6:
                    //OK вийти з облікового запису
                    printMenu();
                    return;

                default:
                    ConsoleIO.printLine("");
                    ConsoleIO.printLine("Невірний номер, повторіть спробу: ");
                    ConsoleIO.printLine("");
            }
        }
    }
}
