package org.booking.controller;

import org.booking.domain.Flight;
import org.booking.domain.User;
import org.booking.libs.ConsoleIO;
import org.booking.service.ReservationService;


public class ReservationController {

    private ReservationService resService;
    public ReservationController(ReservationService resService) {
        this.resService = resService;
    }

    public void printReservationsByUser(User user) {
        try {
            resService.getReservationsByUser(user).stream().forEachOrdered(res -> ConsoleIO.printLine(res.toString()));
        } catch (IllegalStateException ex) {
            ConsoleIO.printLine("У вас ще немає бронювань");
        }
    }

    public void createReservation(User user, Flight flight, int passengers) {
        if (resService.createReservation(user, flight, passengers)) {
            ConsoleIO.printLine(String.format("Бронювання на рейс %s сполученням %s - %s успішно додано",
                    flight.getAirlineCode(), flight.getOriginCity(), flight.getDestinationCity()));
        } else ConsoleIO.printLine("Упс щось пішло не так... Спробуйте знову");
    }
    public void deleteReservation(int id, User user) {
        try {
            resService.deleteReservation(id, user);
            ConsoleIO.printLine(String.format("Бронювання №%d успішно видалено", id));
        } catch (IllegalStateException ex) {
            ConsoleIO.printLine("Не знайшли бронювання з таким номером");
        }
    }
}
